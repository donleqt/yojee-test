import { FC, ReactNode, useState, MutableRefObject, useRef } from 'react';
import { CSSTransition } from 'react-transition-group';
import { lockBodyScroll } from '@/common';

export type ModalControl = {
  open: () => void;
  close: () => void;
};

export type BaseModalProps = {
  title: string;
  refModal?: MutableRefObject<ModalControl | undefined>;
  width?: number;
  canClose?: boolean;
  shouldShowByDefault?: boolean;
  onClose?: () => unknown;
  renderHeader?: () => ReactNode;
  renderFooter?: () => ReactNode;
};

export const BaseModal: FC<BaseModalProps> = ({
  title,
  width,
  canClose = true,
  renderHeader,
  children,
  renderFooter,
  refModal,
  shouldShowByDefault,
  onClose,
}) => {
  const refMask = useRef<HTMLDivElement>(null);
  const [isShowing, setIsShowing] = useState(shouldShowByDefault);
  const modalControl: ModalControl = {
    open() {
      setIsShowing(true);
      lockBodyScroll.lock();
    },
    close() {
      setIsShowing(false);
      lockBodyScroll.unlock();
      onClose?.apply(null);
    },
  };
  const onClickClose = (event) => {
    event.preventDefault();
    modalControl.close();
  };

  if (refModal) {
    // eslint-disable-next-line no-param-reassign
    refModal.current = modalControl;
  }

  return (
    <div className="base-modal">
      <CSSTransition
        nodeRef={refMask}
        in={isShowing}
        timeout={200}
        className="modal-mask"
        unmountOnExit
        onEnter={modalControl.open}
        onExited={modalControl.close}
      >
        <div ref={refMask} className="modal-mask">
          {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
          <div
            role="presentation"
            className="base-modal-overlay"
            onClick={onClickClose}
          />
          <div className="modal-wrapper">
            <div
              className="modal-container"
              style={{
                width: width && `${width}px`,
              }}
            >
              <div className="base-modal-header">
                <h4 className="text-center mb-4">{title}</h4>
                {renderHeader && renderHeader()}
                {canClose && (
                  <button
                    type="button"
                    className="btn btn-link modal-close-btn hand"
                    onClick={onClickClose}
                  >
                    ✕
                  </button>
                )}
              </div>
              <div className="base-modal-body pt-4 px-4 pb-2">{children}</div>
              <div className="base-modal-footer">
                {renderFooter && renderFooter()}
              </div>
            </div>
          </div>
        </div>
      </CSSTransition>
    </div>
  );
};
