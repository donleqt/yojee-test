import { FC, useEffect, useRef } from 'react';

type BodyClassProps = {
  className?: string;
};

export const BodyClass: FC<BodyClassProps> = ({ className }) => {
  const added = useRef<string>();
  const removeAddedClasses = () => {
    if (added.current) {
      added.current.split(' ').forEach((name) => {
        document.body.classList.remove(name);
      });
    }
  };
  const addNewClasses = () => {
    (className || '').split(' ').forEach((name) => {
      if (name) {
        document.body.classList.add(name);
      }
    });
    added.current = className;
  };

  useEffect(() => {
    removeAddedClasses();
    addNewClasses();

    return () => removeAddedClasses();
  });

  return null;
};
