import Head from 'next/head';
import { FC } from 'react';

export const Title: FC<{ children: string }> = ({ children }) => {
  return (
    <Head>
      <title key="title">{children}</title>
      <meta key="twitter:title" name="twitter:title" content={children} />
      <meta key="og:title" property="og:title" content={children} />
    </Head>
  );
};
