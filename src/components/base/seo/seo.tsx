import React, { FC } from 'react';
import { Description } from './description';
import { Meta } from './meta';
import { Title } from './title';

type SeoProps = {
  title: string;
  titleTemplate?: string;
  description?: string;
  image?: string;
};

const seoDefault = {
  title: 'Blog',
  titleTemplate: '%s | Yojee Test',
  description:
    'Another awesome webpage built in ReactJs, Rematch, Nextjs. And other great library.',
};

export const Seo: FC<SeoProps> = ({
  title = seoDefault.title,
  titleTemplate = seoDefault.titleTemplate,
  description = seoDefault.description,
  image,
}) => {
  const finalTitle = titleTemplate.replace('%s', title);

  return (
    <>
      <Title>{finalTitle}</Title>
      <Description>{description}</Description>
      <Meta image={image} />
    </>
  );
};
