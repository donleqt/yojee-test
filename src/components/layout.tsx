import { FC, useEffect } from 'react';
import { BodyClass } from '@/components';
import { installBackToTop } from '@/common';

type LayoutProps = {
  className?: string;
};

export const Layout: FC<LayoutProps> = ({ children, className }) => {
  useEffect(() => {
    installBackToTop();
  }, []);
  return (
    <>
      {className && <BodyClass className={className} />}
      <main>{children}</main>
      <span id="back-to-top" className="back-to-top hand" />
    </>
  );
};
