import React, { FC } from 'react';
import { useDispatch, useStateSelector } from '@/common';
import { BaseModal } from '@/components';
import { TodoForm } from '../todo-form';

export const EditTaskModal: FC = () => {
  const currentTask = useStateSelector((state) => state.todos.currentTask);
  const dispatch = useDispatch();

  return (
    currentTask && (
      <BaseModal
        title="Edit task title"
        onClose={() => dispatch.todos.setEditTask(null)}
        shouldShowByDefault
      >
        <TodoForm
          submitLabel="SAVE"
          defaultValue={currentTask.title}
          onSubmit={(title) => {
            dispatch.todos.updateTask({
              id: currentTask.id,
              title,
            });
            dispatch.todos.setEditTask(null);
          }}
        />
      </BaseModal>
    )
  );
};
