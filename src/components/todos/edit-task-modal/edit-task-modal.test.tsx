import React from 'react';
import { render, screen } from '@/test/test-utils';
import { EditTaskModal } from './edit-task-modal';

describe('EditTaskModal', () => {
  it('Should not render', () => {
    render(<EditTaskModal />);
    const saveButton = screen.queryByText('SAVE');
    expect(saveButton).not.toBeInTheDocument();
  });
});
