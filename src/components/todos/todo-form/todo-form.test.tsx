import React from 'react';
import { render, screen } from '@/test/test-utils';
import { TodoForm } from './todo-form';

describe('TodoTask', () => {
  it('Should render input and button', () => {
    render(<TodoForm onSubmit={() => {}} submitLabel="ADD ITEM" />);
    screen.getByText('ADD ITEM');
    screen.getAllByPlaceholderText('Enter task title here');
  });

  it('Should trigger event', () => {
    const onSubmit = jest.fn();
    render(
      <TodoForm
        onSubmit={onSubmit}
        defaultValue="Testing todo"
        submitLabel="ADD ITEM"
      />
    );
    screen.getByDisplayValue('Testing todo');
    screen.getByText('ADD ITEM').click();
    expect(onSubmit).toBeCalled();
  });
});
