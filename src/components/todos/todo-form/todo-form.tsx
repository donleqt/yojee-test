import React, { FC, useRef } from 'react';

export type TodoFormProps = {
  onSubmit: (title: string) => unknown;
  defaultValue?: string;
  submitLabel: string;
};

export const TodoForm: FC<TodoFormProps> = ({
  onSubmit,
  submitLabel,
  defaultValue,
}) => {
  const input = useRef<HTMLInputElement>(null);

  return (
    <form
      className="todo-form"
      onSubmit={(event) => {
        event.preventDefault();
        const title = (input.current?.value || '').trim();
        if (title && input.current) {
          onSubmit(title);
          input.current.value = '';
        }
      }}
    >
      <div className="form-group d-flex">
        <input
          ref={input}
          type="text"
          className="form-control"
          placeholder="Enter task title here"
          defaultValue={defaultValue}
          required
          aria-required
        />
        <button type="submit" className="btn btn-primary ml-2 flex-shrink-0">
          {submitLabel}
        </button>
      </div>
    </form>
  );
};
