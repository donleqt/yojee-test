import React from 'react';
import { render, screen } from '@/test/test-utils';
import { TodoFilter } from './todo-filter';

describe('TodoFilter', () => {
  it('Should render view buttons', () => {
    render(<TodoFilter />);

    screen.getByText('All');
    screen.getByText('Done');
    screen.getByText('Todo');
  });
});
