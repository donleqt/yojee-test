import React, { FC } from 'react';
import { ViewMode } from '@/types';
import { useDispatch, useStateSelector } from '@/common';
import classNames from 'classnames';

export type TodoFilterProps = {
  className?: string;
};

export const TodoFilter: FC<TodoFilterProps> = () => {
  const dispatch = useDispatch();
  const currentViewMode = useStateSelector((state) => state.todos.viewMode);

  return (
    <div className="todo-filter d-flex my-4 align-items-center justify-content-end">
      <b>Filter:</b>
      <div
        className="btn-group btn-group-sm ml-3"
        role="group"
        aria-label="Todo actions"
      >
        {Object.entries(ViewMode)
          .filter(([key]) => isNaN(Number(key)))
          .map(([key]) => (
            <button
              key={key}
              className={classNames(
                'btn btn-outline-info',
                currentViewMode === ViewMode[key] && 'active'
              )}
              type="button"
              onClick={() => dispatch.todos.setViewMode(ViewMode[key])}
            >
              {key}
            </button>
          ))}
      </div>
    </div>
  );
};
