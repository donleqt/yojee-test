import { useDispatch } from '@/common';
import { Task } from '@/types';
import React, { FC } from 'react';

export type TodoTaskActionsProps = {
  task: Task;
};

export const TodoTaskActions: FC<TodoTaskActionsProps> = ({ task }) => {
  const { isCompleted, id } = task;
  const dispatch = useDispatch();

  return (
    <div
      className="btn-group btn-group-sm ml-auto"
      role="group"
      aria-label="Todo actions"
    >
      <button
        type="button"
        className="btn btn-outline-primary"
        onClick={() => {
          dispatch.todos.setEditTask(task);
        }}
      >
        Edit
      </button>
      {isCompleted ? (
        <button
          type="button"
          className="btn btn-outline-warning"
          onClick={() =>
            dispatch.todos.markAsRedo({
              taskId: id,
            })
          }
        >
          Redo
        </button>
      ) : (
        <button
          type="button"
          className="btn btn-outline-success"
          onClick={() =>
            dispatch.todos.markAsDone({
              taskId: id,
            })
          }
        >
          Done
        </button>
      )}
      <button
        type="button"
        className="btn btn-outline-danger"
        onClick={() =>
          dispatch.todos.deleteTask({
            taskId: id,
          })
        }
      >
        Delete
      </button>
    </div>
  );
};
