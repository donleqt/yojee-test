import React, { FC, memo } from 'react';
import { Task } from '@/types';
import { TodoTaskActions } from './todo-task-actions';

export type TodoTaskProps = {
  task: Task;
};

export const TodoTask: FC<TodoTaskProps> = ({ task }) => {
  return (
    <li className="todo-task list-group-item list-group-item-action d-flex align-items-center">
      {task.isCompleted ? <s>{task.title}</s> : task.title}
      <TodoTaskActions task={task} />
    </li>
  );
};

export const MemoizedTodoTask = memo(TodoTask);
