import React from 'react';
import { render, screen } from '@/test/test-utils';
import { Task } from '@/types';
import { TodoTask } from './todo-task';

describe('TodoTask', () => {
  const task: Task = {
    id: 1,
    title: 'Testing item',
    isCompleted: false,
  };

  it('Should render title and actions', () => {
    render(<TodoTask task={task} />);

    screen.getByText('Testing item');
    screen.getByText('Edit');
    screen.getByText('Done');
    screen.getByText('Edit');
  });

  it('Should render redo', () => {
    render(
      <TodoTask
        task={{
          ...task,
          isCompleted: true,
        }}
      />
    );

    screen.getByText('Redo');
  });
});
