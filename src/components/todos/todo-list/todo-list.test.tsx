import React from 'react';
import { render, screen } from '@/test/test-utils';
import { TodoList } from './todo-list';

describe('TodoList', () => {
  it('Should render example tasks', () => {
    render(<TodoList />);
    screen.getByText('Create your first task.');
  });
});
