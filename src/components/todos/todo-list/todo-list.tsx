import React, { FC, useEffect } from 'react';
import { useDispatch, useStateSelector } from '@/common';
import { MemoizedTodoTask } from '@/components';
import { getTaskList } from '@/store';

export const TodoList: FC = () => {
  const tasks = useStateSelector(getTaskList);
  const dispatch = useDispatch();

  useEffect(() => dispatch.todos.loadTasks(), [dispatch]);

  return (
    <div className="todo-list mt-5">
      {tasks.length ? (
        <ul className="todo-list list-group list-group-item-action mb-5">
          {tasks.map((task) => (
            <MemoizedTodoTask key={task.id} task={task} />
          ))}
        </ul>
      ) : (
        <p className="text-center text-muted mb-4">Empty tasks list.</p>
      )}
    </div>
  );
};
