export const ROUTE_PATHS = {
  DETAIL: '/:slug',
  ABOUT: '/about',
  CATEGORY: '/category',
};
