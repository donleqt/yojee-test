export const BREAKPOINT_MOBILE = '(max-width: 560px)';

export const BREAKPOINT_TABLET = '(min-width: 561px) and (max-width: 1000px)';

export const BREAKPOINT_DESKTOP = '(min-width: 1001px)';
