export const DEFAULT_PAGE_SIZE = 50;

export const SUPPORTED_PAGE_SIZES = [50, 100, 1000];
