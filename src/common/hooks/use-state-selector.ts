import { useSelector } from 'react-redux';

import { State } from '@/store';

export function useStateSelector<TSelected>(
  selector: (state: State) => TSelected
) {
  return useSelector<State, TSelected>(selector);
}
