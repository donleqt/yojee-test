import React, { useContext } from 'react';
import { AppContext } from '@/services';

/**
 * React app context
 */
export const MyAppContext = React.createContext<AppContext | null>(null);

/**
 * Use app context
 *
 * @export
 * @returns {AppContext}
 */
export function useAppContext() {
  const appContext = useContext(MyAppContext);
  return appContext;
}
