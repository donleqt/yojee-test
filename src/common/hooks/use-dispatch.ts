import { useDispatch as reduxUseDispatch } from 'react-redux';
import { Dispatch } from '@/store';

export function useDispatch() {
  return reduxUseDispatch<Dispatch>();
}
