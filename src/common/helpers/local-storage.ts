export const storage = {
  saveData(key: string, data: unknown) {
    if (window) {
      window.localStorage.setItem(key, JSON.stringify(data));
    }
  },
  getData<T>(key: string): T | null {
    const rawData = window && window.localStorage.getItem(key);
    if (rawData) {
      try {
        const data = JSON.parse(rawData) as T;
        return data;
      } catch (err) {
        return null;
      }
    }
    return null;
  },
};
