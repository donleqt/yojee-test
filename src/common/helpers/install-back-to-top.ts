const id = 'back-to-top';

function scrollToTop(duration) {
  // cancel if already on top
  if (
    document.scrollingElement === null ||
    document.scrollingElement?.scrollTop === 0
  )
    return;

  const cosParameter = document.scrollingElement.scrollTop / 2;
  let scrollCount = 0;
  let oldTimestamp = null;

  function step(newTimestamp) {
    if (oldTimestamp !== null && document.scrollingElement !== null) {
      scrollCount += (Math.PI * (newTimestamp - oldTimestamp)) / duration;
      if (scrollCount >= Math.PI) document.scrollingElement.scrollTop = 0;
      else {
        document.scrollingElement.scrollTop =
          cosParameter + cosParameter * Math.cos(scrollCount);
      }
    }
    oldTimestamp = newTimestamp;
    if (document.scrollingElement?.scrollTop !== 0) {
      window.requestAnimationFrame(step);
    }
  }
  window.requestAnimationFrame(step);
}

export function installBackToTop(): () => void {
  const $backToTop = document.getElementById(id);
  if ($backToTop) {
    const scrollTrigger = 100;
    const backToTop = () => {
      const { scrollTop } = document.documentElement;
      if (scrollTop > scrollTrigger) {
        $backToTop.classList.add('show');
      } else {
        $backToTop.classList.remove('show');
      }
    };

    window.addEventListener('scroll', backToTop);
    backToTop();

    $backToTop.onclick = (e) => {
      e.preventDefault();
      scrollToTop(300);
    };

    return () => {
      window.removeEventListener('scroll', backToTop);
    };
  }

  return () => {};
}
