const defaultOptions: ToastrOptions = {
  preventDuplicates: true,
  closeButton: true,
  debug: false,
  positionClass: 'toast-bottom-right',
  onclick: () => {},
  showDuration: 100,
  hideDuration: 100,
  timeOut: 5000,
  extendedTimeOut: 3000,
  showEasing: 'swing',
  hideEasing: 'linear',
  showMethod: 'fadeIn',
  hideMethod: 'fadeOut',
};

const importToastr = () => {
  // @ts-ignore
  import(/* webpackChunkName: "toastr.css" */ 'toastr/build/toastr.css');
  return import(/* webpackChunkName: "toastr" */ 'toastr').then((res) => {
    const toast = res.default;
    toast.options = defaultOptions;
    return toast;
  });
};

export const toast = {
  info: async (message: string, title?: string, options?: ToastrOptions) => {
    if (process.browser) {
      const toastr = await importToastr();
      return toastr.info(message, title || 'Info message', options);
    }
    return null;
  },
  success: async (message: string, title?: string, options?: ToastrOptions) => {
    if (process.browser) {
      const toastr = await importToastr();
      return toastr.success(message, title || 'Success message', options);
    }
    return null;
  },
  error: async (message: string, title?: string, options?: ToastrOptions) => {
    if (process.browser) {
      const toastr = await importToastr();
      return toastr.error(message, title || 'Error message', options);
    }
    return null;
  },
  warning: async (message: string, title?: string, options?: ToastrOptions) => {
    if (process.browser) {
      const toastr = await importToastr();
      return toastr.warning(message, title || 'Notice', options);
    }
    return null;
  },
};
