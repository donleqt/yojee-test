export const lockBodyScroll = {
  isLocked() {
    return document.body.classList.contains('body-locked');
  },
  shouldLock() {
    return false;
  },
  lock() {
    if (lockBodyScroll.shouldLock()) {
      const offset = window.scrollY;
      document.body.style.top = `${offset * -1}px`;
      document.body.classList.add('body-locked');
    }
  },
  unlock() {
    if (lockBodyScroll.shouldLock()) {
      const offset = parseInt(document.body.style.top, 10);
      document.body.classList.remove('body-locked');
      document.body.style.top = '0';
      window.scrollTo(0, offset * -1);
    }
  },
};
