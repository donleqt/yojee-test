export type HTTPResponseCode =
  | 400
  | 401
  | 403
  | 404
  | 405
  | 408
  | 413
  | 414
  | 500
  | 501
  | 202
  | 503
  | 504
  | 0;

export type ApiErrorItem = {
  error: string;
  message: string;
  detail?: string;
};

export type ApiErrorResponse = {
  status: HTTPResponseCode;
  list: ApiErrorItem[];
};

export type ApiErrorResponseMessage = Record<HTTPResponseCode, string>;
