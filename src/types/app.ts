export type EnvironmentVariable = {
  NEXT_PUBLIC_API_URL: string;
  NODE_API_URL: string;
  HOST: string;
};
