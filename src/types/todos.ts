export type Task = {
  id: number;
  title: string;
  isCompleted: boolean;
};

export enum ViewMode {
  All,
  Done,
  Todo,
}
