import { State } from '@/store';

// eslint-disable-next-line @typescript-eslint/naming-convention
export type SSR_DATA = {
  initialState: State;
};
