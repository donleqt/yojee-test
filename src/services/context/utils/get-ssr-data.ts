import { SSR_DATA } from '@/types';

export const getSSRData: () => SSR_DATA = () => {
  if (process.browser) {
    const data: {
      props: {
        SSR_DATA: SSR_DATA;
      };
    } = JSON.parse(document.getElementById('__NEXT_DATA__')?.innerHTML || '{}');
    return data?.props?.SSR_DATA;
  }
  return {
    initialState: {} as SSR_DATA['initialState'],
  };
};
