// @ts-ignore
import { NEXT_DATA } from 'next/dist/next-server/lib/utils';
import { AppContext } from '@/services';

export const setSSRData = (nextData: NEXT_DATA, appContext: AppContext) => {
  // eslint-disable-next-line no-param-reassign
  nextData.props = {
    ...nextData.props,
    SSR_DATA: {
      initialState: appContext?.store.getState() ?? {},
    },
  };
};
