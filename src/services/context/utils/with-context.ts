import { createAppContext, CustomNextPageContext } from '@/services';
import { NextPageContext } from 'next';

export function withContext<T>(
  func: (ctx: CustomNextPageContext) => Promise<T>
) {
  return async (context: NextPageContext) => {
    const appContext = createAppContext(context);
    // @ts-ignore
    context.appContext = appContext;
    return appContext.store.dispatch.context
      .getGlobalSettings()
      .then(() => func(context as CustomNextPageContext))
      .catch(() => func(context as CustomNextPageContext));
  };
}
