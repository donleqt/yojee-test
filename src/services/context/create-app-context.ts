import axios from 'axios';
import { createApi, configureClient } from '@/services';
import { NextPageContext } from 'next';
import { createRematchStore } from '@/store';
import { getSSRData } from './utils';
import { AppContext, CustomNextPageContext } from './types';

let devAppContext: AppContext | null = null;

export const createAppContext = (
  ctx?: NextPageContext,
  state?: Record<string, unknown>
) => {
  if (typeof window !== 'undefined' && devAppContext) {
    return devAppContext;
  }
  const context: AppContext = {} as AppContext;
  const client = axios.create();
  const api = createApi(client);

  const { initialState } = getSSRData();
  const store = createRematchStore(state || initialState, context);

  configureClient(client, store);

  Object.assign(context, {
    api,
    store,
  });

  // For server side only
  if (ctx) {
    (ctx as CustomNextPageContext).appContext = context;
  }

  if (typeof window !== 'undefined') {
    devAppContext = context;
  }

  return context;
};
