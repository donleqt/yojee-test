import { Store } from '@/store';
import { NextPageContext } from 'next';
import { Api } from '../api';

export type AppContext = {
  api: Api;
  store: Store;
};

export type CustomNextPageContext = NextPageContext & {
  appContext: AppContext;
};
