import { createApi } from './creators';

export { createApi, configureClient } from './creators';

export * from './middleware';

export type Api = ReturnType<typeof createApi>;
