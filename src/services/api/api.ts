// eslint-disable-next-line import/no-cycle
import { RequestFactory } from './creators';

export const createRequests = (createRequest: RequestFactory) => ({
  get: {
    global: {
      setting: createRequest<void>('get', '/global'),
    },
  },
  put: {},
  patch: {},
  delete: {},
});
