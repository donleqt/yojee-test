import { AxiosInstance } from 'axios';

// eslint-disable-next-line import/no-cycle
import { createRequests } from '../api';
import { createRequestFactory } from './create-request-factory';

export const createApi = (client: AxiosInstance) => {
  const requestFactory = createRequestFactory(client);
  return createRequests(requestFactory);
};
