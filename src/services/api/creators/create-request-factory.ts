import { Path } from 'path-parser';
import { diff } from 'deep-object-diff';
import { AxiosInstance } from 'axios';

type ApiMethods = 'get' | 'post' | 'put' | 'patch' | 'delete';

type ApiOptions<T> = {
  baseURL?: string;
  parseResponse?: (data: T) => T;
};

export const createRequestFactory =
  (apiClient: AxiosInstance) =>
  <Response, Request = void>(
    method: ApiMethods,
    uri: string,
    options: ApiOptions<Response> = {}
  ) =>
  (request: Request) => {
    const path = new Path(uri);
    const url = path.build(request);
    const usedPayload = path.partialTest(url);
    const payload = diff(
      usedPayload || {},
      request || {}
    ) as unknown as Request;
    const payloadKey = method === 'get' ? 'params' : 'data';

    return apiClient
      .request<Response>({
        url,
        method,
        baseURL: options.baseURL,
        [payloadKey]: payload,
      })
      .then((response) => {
        if (options.parseResponse) {
          return options.parseResponse(response.data);
        }
        return response.data;
      });
  };

export type RequestFactory = ReturnType<typeof createRequestFactory>;
