import { AxiosInstance } from 'axios';

import { Store } from '@/store';

import { ApiErrorHandler, applyClientMiddleware } from '../middleware';

export const configureClient = (
  client: AxiosInstance,
  store: Store,
  errorHandler?: ApiErrorHandler
) => {
  const API_URL = process.browser
    ? process.env.NEXT_PUBLIC_API_URL
    : process.env.NODE_API_URL;
  const API_VERSION = process.env.NEXT_PUBLIC_API_VERSION || '';

  // eslint-disable-next-line no-param-reassign
  client.defaults.baseURL = [API_URL, API_VERSION].filter(Boolean).join('/');

  // Set middlewares and error handler
  applyClientMiddleware(client, store, errorHandler);

  return client;
};
