import { AxiosInstance, AxiosError } from 'axios';
import { Store } from '@/store';
import { toast } from '@/common';

export type ApiErrorHandler = (error: AxiosError) => never | void;

export const apiErrorNotification = (client: AxiosInstance, _store: Store) => {
  client.interceptors.response.use(undefined, (error: AxiosError) => {
    const errors = error?.response?.data?.errors;
    if (!errors || errors.length === 0) {
      const errorMessage = error.message || 'General Error';
      toast.error(errorMessage, 'API error');
    }
    return Promise.reject(error);
  });
};
