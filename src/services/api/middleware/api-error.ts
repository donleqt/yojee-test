import { AxiosInstance, AxiosError } from 'axios';
import { Store } from '@/store';

export type ApiErrorHandler = (error: AxiosError) => never | void;

export const apiError = (
  client: AxiosInstance,
  _store: Store,
  errorHandler: ApiErrorHandler = () => {}
) => {
  client.interceptors.response.use(undefined, (error: AxiosError) => {
    if (error.response?.status === 401) {
      // dispatch(logoutUser());
    } else {
      errorHandler(error);
    }
    return Promise.reject(error);
  });
};
