import { AxiosInstance } from 'axios';

import { Store } from '@/store';

import { apiError, ApiErrorHandler } from './api-error';
import { debugLogErrors } from './debug-log-errors';
import { loadingIndicator } from './loading-indicator';
import { defaultRequestHeaders } from './default-request-headers';
import { apiErrorNotification } from './api-error-notification';

export const applyClientMiddleware = (
  client: AxiosInstance,
  store: Store,
  errorHandler?: ApiErrorHandler
) => {
  const {
    context: { isSSR },
  } = store.getState();

  apiError(client, store, errorHandler);
  debugLogErrors(client, store);
  defaultRequestHeaders(client, store);

  if (isSSR) return;

  apiErrorNotification(client, store);
  loadingIndicator(client);
};
