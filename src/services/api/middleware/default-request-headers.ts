import { AxiosInstance } from 'axios';

import { Store } from '@/store';

export const defaultRequestHeaders = (client: AxiosInstance, store: Store) => {
  client.interceptors.request.use((config) => {
    const {
      context: { lang },
    } = store.getState();

    const headers = {
      'Content-Language': lang,
    };

    return {
      ...config,
      headers: {
        ...config.headers,
        ...headers,
      },
    };
  });
};
