import { AxiosInstance } from 'axios';

import { Store } from '@/store';

export const debugLogErrors = (client: AxiosInstance, store: Store) => {
  const {
    context: {
      env: { DEBUG },
      isSSR,
    },
  } = store.getState();

  if (isSSR || DEBUG !== 'true') return;

  client.interceptors.response.use(undefined, (error) => {
    // eslint-disable-next-line no-console
    console.error('[API CLIENT ERROR]\n', error, '\n');
    return Promise.reject(error);
  });
};
