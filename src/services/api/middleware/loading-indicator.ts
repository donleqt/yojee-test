import {
  AxiosError,
  AxiosInstance,
  AxiosResponse,
  AxiosRequestConfig,
} from 'axios';

export const loadingIndicator = async (client: AxiosInstance) => {
  if (!process.browser) {
    return;
  }
  const { default: nProgress } = await import('nprogress');

  let requestCount = 0;

  let stopTimeoutId: NodeJS.Timeout | null = null;

  const start = (config: AxiosRequestConfig) => {
    if (!requestCount) nProgress.start();

    if (stopTimeoutId) {
      clearTimeout(stopTimeoutId);
      stopTimeoutId = null;
    }

    requestCount += 1;

    return config;
  };

  const stop = (response: AxiosResponse | AxiosError) => {
    requestCount -= 1;

    if (!requestCount) stopTimeoutId = setTimeout(() => nProgress.done(), 300);

    return response instanceof Error
      ? Promise.reject(response)
      : Promise.resolve(response);
  };

  client.interceptors.request.use(start);

  client.interceptors.response.use(stop, stop);
};
