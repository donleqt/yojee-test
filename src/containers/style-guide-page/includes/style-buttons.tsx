import React from 'react';
import { StyleSection } from './style-section';

const ButtonTypes = ['primary', 'secondary', 'warning', 'danger', 'success'];

/**
 * Style buttons
 *
 */
export const StyleButtons = () => (
  <StyleSection title="Buttons">
    <h3>State buttons</h3>
    <div className="button-list">
      {/* Render buttons with solid states */}
      {ButtonTypes.map((state) => (
        <button
          key={state}
          type="button"
          className={`btn btn-${state} mb-2 mr-4`}
        >
          btn-
          {state}
        </button>
      ))}
    </div>

    <div className="button-list is-outline">
      {/* Render buttons with outline states */}
      {ButtonTypes.map((state) => (
        <button
          key={state}
          type="button"
          className={`btn btn-outline-${state}`}
        >
          btn-outline-
          {state}
        </button>
      ))}
    </div>

    <hr className="my-5" />

    <div className="button-list is-large">
      {/* Render buttons with large states */}
      {ButtonTypes.map((state) => (
        <button key={state} type="button" className={`btn btn-lg btn-${state}`}>
          Listen Now
        </button>
      ))}
    </div>
  </StyleSection>
);
