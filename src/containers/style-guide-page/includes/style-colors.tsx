import React from 'react';
import { StyleSection } from './style-section';

/**
 * JSX list colors
 *
 * @param {{ title: string, colors: string[] }} props
 */
const ColorList = ({ title, colors }: { title: string; colors: string[] }) => (
  <div className="color-list">
    <h3>{title}</h3>
    <div className="row">
      {colors.map((color) => (
        <div key={color} className="col-md-3 col-lg-2 col-4">
          <div className={`square bg-${color}`} />
          <p>
            $color-
            {color}
          </p>
        </div>
      ))}
    </div>
  </div>
);

/**
 * Style colors
 *
 */
export const StyleColors = () => (
  <StyleSection title="Colors">
    <ColorList
      title="State colors"
      colors={['primary', 'info', 'secondary', 'warning', 'danger', 'success']}
    />
  </StyleSection>
);
