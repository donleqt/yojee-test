import React from 'react';
import { StyleSection } from './style-section';
import StyleFontsData from './style-fonts-data.json';

/**
 * Render the font family section
 *
 * @param {{ className: string, title: string }} { className, title }
 */
export const StyleFontsSection = ({
  className,
  title,
}: {
  className: string;
  title: string;
}) => (
  <>
    <h4 className="text-brand bold">{title}</h4>
    <div className="mb-5" />
    <div className={`headings ${className}`}>
      {/* eslint-disable-next-line @typescript-eslint/no-explicit-any */}
      {['h1', 'h2', 'h3', 'h4', 'h5', 'h6'].map((HeadingTag: any) => (
        <HeadingTag className={className} key={HeadingTag}>
          {HeadingTag.toUpperCase()} - {StyleFontsData.heading}
        </HeadingTag>
      ))}
      <p>
        <b>Lorem Ipsum</b>
        &nbsp;
        {StyleFontsData.bodyText}
      </p>
    </div>
    <div className="pt-3" />
    <div className="mt-5" />
  </>
);

/**
 * Style guide for fonts
 *
 */
export const StyleFonts = () => (
  <StyleSection title="Typography">
    <StyleFontsSection className="font-text" title="Font Text" />
    <StyleFontsSection className="font-heading" title="Font Heading" />
  </StyleSection>
);
