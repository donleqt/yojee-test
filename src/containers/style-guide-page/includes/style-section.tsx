import React from 'react';

/**
 * StyleSection props
 *
 * @interface StyleSectionProps
 */
interface StyleSectionProps {
  title: string;
  children: React.ReactNode;
}

/**
 * Section component used in style guide page.
 * Including a simple title and children
 *
 * @param {StyleSectionProps} { title, children }
 */
export const StyleSection = ({ title, children }: StyleSectionProps) => (
  <section className={`container section-${title && title.toLowerCase()}`}>
    <h2>{title}</h2>
    <div className="section-content">{children}</div>
  </section>
);
