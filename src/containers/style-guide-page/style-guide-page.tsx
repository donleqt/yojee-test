import React from 'react';
import { Seo } from '@/components';
import { StyleButtons, StyleColors, StyleFonts } from './includes';

/**
 * Style guide page, display colors, fonts, etc
 *
 * @export
 * @returns
 */
export const StyleGuidePage = () => {
  return (
    <div className="style-guide-page">
      <Seo title="Style Guide" />
      <section className="pt-5 page-intro container">
        <div className="header-logo-wrapper">
          <h1 className="font-heading">Yojee Test</h1>
        </div>
      </section>
      <StyleColors />
      <StyleButtons />
      <StyleFonts />
    </div>
  );
};
