import { useDispatch } from '@/common';
import { Layout } from '@/components';
import {
  TodoList,
  EditTaskModal,
  TodoForm,
  TodoFilter,
} from '@/components/todos';
import React, { FC } from 'react';

export type TodoPageProps = {
  className?: string;
};

export const TodoPage: FC<TodoPageProps> = () => {
  const dispatch = useDispatch();

  return (
    <Layout>
      <div className="container my-5">
        <h1>Todo List</h1>
        <div className="lead">Get things done, one item at a time.</div>
        <TodoFilter />
        <TodoList />
        <EditTaskModal />
        <TodoForm
          submitLabel="ADD ITEM"
          onSubmit={(title) => dispatch.todos.addTask({ title })}
        />
      </div>
    </Layout>
  );
};
