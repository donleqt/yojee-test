// eslint-disable-next-line import/no-extraneous-dependencies
import { render } from '@testing-library/react';
import { createAppContext } from '@/services';
import { MyAppContext } from '@/common';
import { Provider } from 'react-redux';

// Add in any providers here if necessary:
// (ReduxProvider, ThemeProvider, etc)
const makeStateProviders =
  (initialState) =>
  // eslint-disable-next-line react/display-name
  ({ children }) => {
    const appContext = createAppContext(undefined, initialState);

    return (
      <MyAppContext.Provider value={appContext}>
        <Provider store={appContext.store}>{children}</Provider>
      </MyAppContext.Provider>
    );
  };

const customRender = (ui, options = {}) =>
  render(ui, { wrapper: makeStateProviders({}), ...options });

export const makeRenderWithState =
  (state) =>
  (ui, options = {}) =>
    render(ui, { wrapper: makeStateProviders(state), ...options });

// re-export everything
// eslint-disable-next-line import/no-extraneous-dependencies
export * from '@testing-library/react';

// override render method
export { customRender as render };
