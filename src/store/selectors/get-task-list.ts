import { ViewMode } from '@/types';
import { createSelector } from 'reselect';
import { State } from '../create-rematch-store';

export const getTaskList = createSelector(
  (state: State) => state.todos.tasks,
  (state: State) => state.todos.viewMode,
  (taskList, currentViewMode) => {
    switch (currentViewMode) {
      case ViewMode.All:
        return taskList;
      case ViewMode.Done:
        return taskList.filter((e) => e.isCompleted);
      case ViewMode.Todo:
        return taskList.filter((e) => !e.isCompleted);
      default:
        return taskList;
    }
  },
  {
    memoizeOptions: {
      maxSize: 3,
    },
  }
);
