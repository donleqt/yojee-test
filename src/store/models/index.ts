import { Models } from '@rematch/core';
import { context } from './context';
import { todos } from './todos';

export const models: RootModel = {
  context,
  todos,
};

export interface RootModel extends Models<RootModel> {
  context: typeof context;
  todos: typeof todos;
}
