import { createModel } from '@rematch/core';
import { RootModel } from '@/store/models';

export const context = createModel<RootModel>()({
  state: {
    appKey: 0,
    isSSR: false,
    lang: 'en',
    env: {
      DEBUG: 'false',
    },
    setting: {
      favicon: null,
      siteName: '',
      defaultSeo: {
        metaTitle: '',
        metaDescription: '',
        shareImage: '',
      },
      facebook: '',
      instagram: '',
      youtube: '',
    },
  },
  reducers: {
    refreshApp(state) {
      return {
        ...state,
        appKey: state.appKey + 1,
      };
    },
    setSetting(state, payload) {
      return {
        ...state,
        setting: payload,
      };
    },
  },
  effects: () => ({
    async getGlobalSettings() {
      return true;
    },
  }),
});
