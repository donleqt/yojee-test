import { createModel } from '@rematch/core';
import { RootModel } from '@/store/models';
import { Task, ViewMode } from '@/types';
import { storage } from '@/common';

const STORAGE_KEY = 'tasks';

const saveTasks = (data: Task[]) => {
  storage.saveData(STORAGE_KEY, data);
};

const loadTasks = () => {
  return storage.getData<Task[]>(STORAGE_KEY);
};

const exampleTasks: Task[] = [
  {
    id: 1,
    title: 'Create your first task.',
    isCompleted: false,
  },
];

const initialState: {
  currentTask: Task | null;
  viewMode: ViewMode;
  tasks: Task[];
} = {
  currentTask: null,
  viewMode: ViewMode.All,
  tasks: [],
};

export const todos = createModel<RootModel>()({
  state: initialState,
  reducers: {
    addTask(state, { title }: { title: string }) {
      state.tasks.push({
        id: Date.now(),
        title,
        isCompleted: false,
      });
      saveTasks(state.tasks);
    },
    deleteTask(state, { taskId }: { taskId: number }) {
      const taskIndex = state.tasks.findIndex((e) => e.id === taskId);
      state.tasks.splice(taskIndex, 1);
      saveTasks(state.tasks);
    },
    markAsDone(state, { taskId }: { taskId: number }) {
      const task = state.tasks.find((e) => e.id === taskId);
      if (task) {
        task.isCompleted = true;
        saveTasks(state.tasks);
      }
    },
    markAsRedo(state, { taskId }: { taskId: number }) {
      const task = state.tasks.find((e) => e.id === taskId);
      if (task) {
        task.isCompleted = false;
        saveTasks(state.tasks);
      }
    },
    updateTask(state, { title, id }: Pick<Task, 'id' | 'title'>) {
      const task = state.tasks.find((e) => e.id === id);
      if (task) {
        task.title = title;
        saveTasks(state.tasks);
      }
    },
    setEditTask(state, payload: Task | null) {
      return {
        ...state,
        currentTask: payload,
      };
    },
    setTasksList(state, payload: Task[]) {
      return {
        ...state,
        tasks: payload,
      };
    },
    setViewMode(state, payload: ViewMode) {
      return {
        ...state,
        viewMode: payload,
      };
    },
  },
  effects: (dispatch) => ({
    loadTasks() {
      const tasks = loadTasks() || exampleTasks;
      dispatch.todos.setTasksList(tasks);
    },
  }),
});
