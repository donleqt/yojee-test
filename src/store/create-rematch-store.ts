import { AppContext } from '@/services';
import immerPlugin from '@rematch/immer';
import { init, RematchDispatch, RematchRootState } from '@rematch/core';
import { models, RootModel } from './models';

export const createRematchStore = (
  initialState: Record<string, unknown>,
  context: AppContext
) => {
  const store = init({
    models,
    redux: {
      initialState,
    },
    plugins: [immerPlugin<RootModel>()],
  });

  store.context = context;
  store.dispatch.store = store;

  return store;
};

export type Store = ReturnType<typeof createRematchStore>;

export type State = ReturnType<Store['getState']>;

export type Dispatch = RematchDispatch<RootModel> & { store: Store };

export type RootState = RematchRootState<RootModel>;
