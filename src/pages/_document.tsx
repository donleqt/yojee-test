import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from 'next/document';
import { CustomNextPageContext, setSSRData } from '@/services';

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const nextContext = ctx as unknown as CustomNextPageContext;
    const originalRenderPage = ctx.renderPage;

    // Override the default render page to inject app context
    ctx.renderPage = (options) => {
      return originalRenderPage({
        ...options,
        enhanceApp: (App) => {
          const NewApp: typeof App = (props) => (
            // @ts-ignore
            <App {...props} appContext={nextContext.appContext} />
          );
          return NewApp;
        },
      });
    };

    const initialProps = await Document.getInitialProps(ctx);

    return {
      ...initialProps,
      appContext: nextContext.appContext,
    };
  }

  constructor(props) {
    super(props);
    // eslint-disable-next-line no-underscore-dangle
    setSSRData(this.props.__NEXT_DATA__, props.appContext);
  }

  render() {
    return (
      <Html lang="en">
        <Head>
          <link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/icons/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/icons/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="//icons/favicon-16x16.png"
          />
          <link rel="manifest" href="/manifest.json" />
          <link
            rel="mask-icon"
            href="/svgs/safari-pinned-tab.svg"
            color="#6366f1"
          />
          <meta name="theme-color" content="#ffffff" />
          <script src="https://unpkg.com/phosphor-icons" defer />
        </Head>
        <body className="overflow-auto">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
