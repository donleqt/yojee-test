import type { AppProps } from 'next/app';
import React from 'react';
import { Seo } from '@/components';
import { MyAppContext, setUpFetchBar } from '@/common';
import { Provider } from 'react-redux';
import { AppContext, createAppContext } from '@/services';

import '@/scss/thirtparty/bootstrap.scss';
import '@/scss/app.scss';

setUpFetchBar();

function MyApp({
  Component,
  pageProps,
  appContext = createAppContext(),
}: AppProps & {
  appContext: AppContext;
}) {
  return (
    <MyAppContext.Provider value={appContext}>
      <Provider store={appContext.store}>
        <Seo title="Todo list" description="An empoloyee tool." />
        <Component {...pageProps} />
      </Provider>
    </MyAppContext.Provider>
  );
}

export default MyApp;
