module.exports = {
  setupFilesAfterEnv: ['./jest.setup.ts'],
  testEnvironment: 'jsdom',
  modulePathIgnorePatterns: [
    '<rootDir>/cypress/',
    '<rootDir>/.e2e-examples/',
    '<rootDir>/.cache/',
  ],
  moduleDirectories: ['node_modules'],
  moduleNameMapper: {
    '^@(/.*)$': '<rootDir>/src$1',
  },
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': ['babel-jest', { presets: ['next/babel'] }],
  },
};
