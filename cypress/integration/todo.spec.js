/// <reference types="cypress" />

describe('Test to-do app', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('displays one todo items by default', () => {
    cy.get('.todo-task').should('have.length', 1);
    cy.get('.todo-task')
      .first()
      .should('contain.text', 'Create your first task.');
  });

  it('can add new todo items', () => {
    const newItem = 'Feed the cat';

    cy.get('.todo-form input').type(`${newItem}{enter}`);

    cy.get('.todo-task')
      .should('have.length', 2)
      .last()
      .should('contain.text', newItem);

    cy.reload();

    cy.get('.todo-task')
      .should('have.length', 2)
      .last()
      .should('contain.text', newItem);
  });

  it('can check off an item as completed', () => {
    cy.contains('Create your first task.').contains('Done').click();
    cy.contains('Create your first task.')
      .parent()
      .find('s')
      .should('have.length', 1);
  });

  it('can delete an item', () => {
    cy.contains('Create your first task.').contains('Delete').click();
    cy.get('.todo-task').should('have.length', 0);
  });
});
