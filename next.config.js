const globImporter = require('node-sass-glob-importer');

module.exports = {
  reactStrictMode: true,
  sassOptions: {
    importer: globImporter(),
  },
};
