# Yojee Test - Don Le Quy.

Below is the test information:

Create a to-do list application with ReactJS.
This application can work without the BE side.

Objectives:
- Create react js app skeleton structure
- Create one screen application for list/create/delete/update/filter to-dos
- Define what tools you would use (no need to integrate them)
- Describe what steps would be needed to deploy it
- Nice to have: CI, CD steps
- UI/UX not a main focus for this assignment
- Describe how you would solve performance issues

## Overview
Online Preview: [https://yojee-test.herokuapp.com](https://yojee-test.herokuapp.com).

![screenshot](/screenshot.png "Screenshot").

## Technologies:

Nextjs, Typescript, React, ESlint, Prettier, Redux with [RematchJs](https://rematchjs.org/).
Bootstrap, SASS.

Heroku, Gitlab CI/CD, Jest for unit tests.

## Performance tweaks.

1. Memoized Task component.

When we edit or mark a task as completed. React would re-render the whole list causing non-edited tasks also re-render. To avoid this I used React.memo, so whenever a task changes only that Task re-renders.

2. Filtering logic is inside redux selector (`reselect`).

Tasks are displayed based on view mode (all, todo, done). To minimize Array.filter execution. I put the filtering logic into the selector (`createSelector`). So it only runs when the view mode or task list change.

3. Render TodoForm, EditModal, TodoFilter independently.

Apparently, the most re-rendering component is TodoList. So I designed to render other components independent on TodoList. So that whenever we interact with the task list doesn't cause form, modal, filter re-render.

4. Other minor tips:

- Used `task.id` for `key` insteads of array index.
- Used React production build when we deploy.
- Used `reselect` to create selector.

## Skeleton featured:

1. React Hooks: `useAppContext`, `useStateSelector`, `useStateDispatch`
2. Used Redux to manage state.
3. Unit tests by using jest. (`$ yarn test`)
4. Used SASS as a CSS Preprocessor for app's style.
5. Gitlab CI/CD setup.
6. Used Husky to add pre-commit hooks of Eslint and Typescript types checking.
7. NextJs, Typescript, ESlint, SSR.
8. Ready to use Api client.

## Todo if having free time
1. Implement e2e tests by using Cypress.
2. Update CI/CD pipline to run e2e tests.
3. Update CI/CD pipline to generate preview app per merge request.
4. Client side version. Release tags: e.g. v0.0.1, v1.0.2 etc...

## How to deploy using Gitlab CI/CD.

Please create a new gitlab repo and follow these steps:

1. Register a Heroku account.
2. Get your Heroku API key, add to your Gitlab setting as an env variable HEROKU_API_KEY
3. Create a Heroku app, add to your Gitlab setting as an env variable HEROKU_APP_PRODUCTION (equals to app name)
4. Commit and push to the master. CI/CD would run build/test and deploy the app to your Heroku container.

![pipeline](/pipeline.png "Example of the pipeline").


## Development guide

#### Install and develop

```bash
yarn && yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

#### Unit tests

```bash
yarn test
```


#### Start for production

```bash
yarn && yarn build && yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

----


## Components

Components are designed following SOLID principles to keep things simple and extendable.
               
##### Main components

| Components                      | Description                   |
| ------------------------------- | ----------------------------- |
| TodoTask                           | Display todo task.                         |
| TodoTaskActions                        | Contains Edit/Done/Redo/Delete actions of a task.                          |
| TodoForm                    | Displays and allows inputs Todo data.                          |
| EditTaskModal             | Contains TodoForm, handles the logic to edit a task data.                         |
| TodoList                    | Task list            |
| TodoPage                    | Contains the list. Define page SEO's info.                          |
| StyleGuildePage                    | Displays  the design system: colors, fonts, buttons... https://yojee-test.herokuapp.com/style-guide                          |


##### Other components

| Components                      | Description                   |
| ------------------------------- | ----------------------------- |
| Seo                           | Declare SEO title and meta data for pages.           |
| BodyClass             | Supports adding CSS class to \<body>                        |
| BaseModal                    | Receives and render its children inside a modal.                         |
